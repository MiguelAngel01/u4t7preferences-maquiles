package dam.androidmiguelangel.u4t7preferences;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.TextView;

public class NewActivity extends AppCompatActivity {

    private TextView tvPreferences;
    private final String MYPREFS = "MyPrefs";
    SharedPreferences myPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new);

        setUI();
    }

    private void setUI(){

        tvPreferences = findViewById(R.id.tvPreferences);

        Bundle bundle = this.getIntent().getExtras();
        if (bundle.getBoolean("sharedPreferences")){
            myPreferences = getSharedPreferences(MYPREFS, MODE_PRIVATE);
        } else if (bundle.getBoolean("preferences")){
            myPreferences = getPreferences(MODE_PRIVATE);
        } else if (bundle.getBoolean("defaultPreferences")){
            myPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        }

        String preferences = "PLAYER: " + myPreferences.getString("PlayerName", "unknown") + "\n" +
                "SCORE: " + String.valueOf(myPreferences.getInt("Score", 0)) + "\n" +
                "LEVEL: " + String.valueOf(myPreferences.getInt("Level", 0) + 1) + "\n" +
                "DIFFICULTY " + String.valueOf(myPreferences.getInt("Difficulty", 0)) + "\n" +
                "SOUND" + myPreferences.getBoolean("Sound", false) + "\n" +
                "BG COLOR: " + String.valueOf(myPreferences.getInt("BackgroundColor", 0) + 1);

        tvPreferences.setText(preferences);

    }

}