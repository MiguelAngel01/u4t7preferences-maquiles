package dam.androidmiguelangel.u4t7preferences;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

public class MainActivity extends AppCompatActivity {

    private final String MYPREFS = "MyPrefs";
    private EditText etPlayerName;
    private Spinner spinnerLevel;
    private EditText etScore;
    private Button btQuit;
    private CheckBox cbSound;
    private Spinner spinnerBackground;
    private RadioGroup rgDifficulty;
    private RadioGroup rgPreferences;
    private RadioButton rbEasy;
    private RadioButton rbNormal;
    private RadioButton rbHard;
    private RadioButton rbVeryHard;
    private RadioButton rbExpert;
    private RadioButton rbSharedPreferences;
    private RadioButton rbPreferences;
    private RadioButton rbDefaultPreferences;
    private Button btShowPreferences;
    private ConstraintLayout rootLayout;
    private Context context = this;

    SharedPreferences myPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI() {

        rootLayout = findViewById(R.id.rootLayout);

        etPlayerName = findViewById(R.id.etPlayerName);

        spinnerLevel = findViewById(R.id.spinnerLevel);
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this, R.array.levels, android.R.layout.simple_spinner_item);

        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLevel.setAdapter(spinnerAdapter);

        etScore = findViewById(R.id.etScore);

        cbSound = findViewById(R.id.cbSound);
        rgDifficulty = findViewById(R.id.rgDifficulty);
        rgPreferences = findViewById(R.id.rgPreferences);
        rbEasy = findViewById(R.id.rbEasy);
        rbNormal = findViewById(R.id.rbNormal);
        rbHard = findViewById(R.id.rbHard);
        rbVeryHard = findViewById(R.id.rbVeryHard);
        rbExpert = findViewById(R.id.rbExpert);
        rbSharedPreferences = findViewById(R.id.rbSharedPreferences);
        rbPreferences = findViewById(R.id.rbPreferences);
        rbDefaultPreferences = findViewById(R.id.rbDefaultPreferences);

        spinnerBackground = findViewById(R.id.spinnerBackground);
        ArrayAdapter<CharSequence> spinnerAdapter1 = ArrayAdapter.createFromResource(this, R.array.backgroundColors, android.R.layout.simple_spinner_item);

        spinnerAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerBackground.setAdapter(spinnerAdapter1);

        btQuit = findViewById(R.id.btQuit);
        btQuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        spinnerBackground.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (spinnerBackground.getSelectedItem().toString()){
                    case "Red":
                        rootLayout.setBackgroundColor(getResources().getColor(R.color.Red));
                        break;
                    case "Green":
                        rootLayout.setBackgroundColor(getResources().getColor(R.color.Green));
                        break;
                    case "Blue":
                        rootLayout.setBackgroundColor(getResources().getColor(R.color.Blue));
                        break;
                    case "Orange":
                        rootLayout.setBackgroundColor(getResources().getColor(R.color.Orange));
                        break;
                    case "White":
                        rootLayout.setBackgroundColor(getResources().getColor(R.color.White));
                        break;
                    default:
                        rootLayout.setBackgroundColor(getResources().getColor(R.color.White));
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btShowPreferences = findViewById(R.id.btShowPreferences);
        btShowPreferences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, NewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putBoolean("sharedPreferences", rbSharedPreferences.isChecked());
                bundle.putBoolean("preferences", rbPreferences.isChecked());
                bundle.putBoolean("defaultPreferences", rbDefaultPreferences.isChecked());
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();

        int checkedId = rgPreferences.getCheckedRadioButtonId();
        if (checkedId == rbSharedPreferences.getId()){
            myPreferences = getSharedPreferences(MYPREFS, MODE_PRIVATE);
        } else if (checkedId == rbPreferences.getId()){
            myPreferences = getPreferences(MODE_PRIVATE);
        } else if (checkedId == rbDefaultPreferences.getId()){
            myPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        }

        SharedPreferences.Editor editor = myPreferences.edit();

        editor.putString("PlayerName", etPlayerName.getText().toString());
        editor.putInt("Level", spinnerLevel.getSelectedItemPosition());
        editor.putInt("Score", Integer.parseInt(etScore.getText().toString()));
        editor.putInt("BackgroundColor", spinnerBackground.getSelectedItemPosition());
        editor.putBoolean("Sound", cbSound.isChecked());
        editor.putInt("Difficulty", rgDifficulty.getCheckedRadioButtonId());
        editor.putInt("Preferences", rgPreferences.getCheckedRadioButtonId());

        editor.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences myPreferences = getSharedPreferences(MYPREFS, MODE_PRIVATE);

        etPlayerName.setText(myPreferences.getString("PlayerName", "unknown"));
        spinnerLevel.setSelection(myPreferences.getInt("Level", 0));
        etScore.setText(String.valueOf(myPreferences.getInt("Score", 0)));
        spinnerBackground.setSelection(myPreferences.getInt("BackgroundColor", 0));
        cbSound.setChecked(myPreferences.getBoolean("Sound", false));

        int difficultyId = myPreferences.getInt("Difficulty", rbNormal.getId());
        if (difficultyId == rbEasy.getId()){
            rbEasy.setChecked(true);
        } else if (difficultyId == rbNormal.getId()){
            rbNormal.setChecked(true);
        } else if (difficultyId == rbHard.getId()){
            rbHard.setChecked(true);
        } else if (difficultyId == rbVeryHard.getId()){
            rbVeryHard.setChecked(true);
        } else if (difficultyId == rbExpert.getId()){
            rbExpert.setChecked(true);
        }

        int checkedId = myPreferences.getInt("Preferences", rbSharedPreferences.getId());
        if (checkedId == rbSharedPreferences.getId()){
            rbSharedPreferences.setChecked(true);
        } else if (checkedId == rbPreferences.getId()){
            rbPreferences.setChecked(true);
        } else if (checkedId == rbDefaultPreferences.getId()){
            rbDefaultPreferences.setChecked(true);
        }

        switch (spinnerBackground.getSelectedItem().toString()){
            case "Red":
                rootLayout.setBackgroundColor(getResources().getColor(R.color.Red));
                break;
            case "Green":
                rootLayout.setBackgroundColor(getResources().getColor(R.color.Green));
                break;
            case "Blue":
                rootLayout.setBackgroundColor(getResources().getColor(R.color.Blue));
                break;
            case "Orange":
                rootLayout.setBackgroundColor(getResources().getColor(R.color.Orange));
                break;
            case "White":
                rootLayout.setBackgroundColor(getResources().getColor(R.color.White));
                break;
            default:
                rootLayout.setBackgroundColor(getResources().getColor(R.color.White));
                break;
        }


    }

}
